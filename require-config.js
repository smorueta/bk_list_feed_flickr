requirejs.config({
    //baseUrl: '/projects/flickr',
    baseUrl: '/',
    paths: {
        jquery: "libs/jquery-1.11.3",
        underscore: "libs/underscore-1.8.3",
        backbone: "libs/backbone-1.2.1",
        moment: "libs/moment-2.10.3"
    },
    shim: {
        jquery: {
            exports: '$'
        },
        underscore: {
            exports: '_'
        },
        backbone: {
            deps: ["underscore", "jquery"],
            exports: 'backbone'
        },
        moment: {
            exports: 'moment'
        }

    }
});
// Load the main app module to start the app
requirejs(["main"]);