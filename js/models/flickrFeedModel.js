define(['backbone', 'moment'],function(Backbone, moment){
	
	return Backbone.Model.extend({
		
		parse:function(response,options){
			var value;
			for (key in response){

				if (key === 'published') {
					value = this.getParseDate(response[key]);
				} if (key === 'tags'){
					value = this.getParseTags(response[key]);
				} else{
					value = response[key];
				}
				
				this.set(key, value);
			}
		},

		getParseDate:function(date){
			var parsedDate = moment(date).format('Do MMMM  YYYY [at] h:mm');
			return parsedDate;
		},

		getParseTags:function(tags){
			var arrTags = tags.split(' '),
				parsedTags = _.each(arrTags,function(item, index){
					arrTags[index] = item.replace(item,"<span class=\"tag link\" data-id=" + item + ">" + item + "</span>");
				});
				
			return arrTags;
		}

	});
});
