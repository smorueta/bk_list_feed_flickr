define(["backbone","js/models/flickrFeedModel"],function(Backbone,FlickrFeedModel){
	
	return Backbone.Collection.extend({
		
		model: FlickrFeedModel,
		url: 'https://api.flickr.com/services/feeds/photos_public.gne?tagmode=all&format=json',
		
		initialize:function(){
			this.fetch({success:this.onSuccessFetch, error:this.onErrorFetch});
		},

	    sync: function(method, collection, options) {
	    	options.dataType = "jsonp";
	    	options.jsonpCallback = "jsonFlickrFeed";
	    	return Backbone.sync(method, collection, options);
	    },
	    
	    parse: function(response) {
        	return response.items;
    	},

		onSuccessFetch:function(){
    		console.log('success fetching data');
		},

    	onErrorFetch:function(){
    		console.log('error fetching data');
    	}

	});
});
