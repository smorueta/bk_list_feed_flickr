define(["js/views/FlickrFeedMain", "js/collections/flickrFeedCollection"], function(FlickrFeedMain, FlickrFeedCollection){
     
	var FlickrFeedModule = function(){	
		
		function initialize(){
			var collection = new FlickrFeedCollection();
			collection.on('sync', LoadFlickrFeedMain);
		};

		function LoadFlickrFeedMain (collection){
			new FlickrFeedMain({ el: $("#content-js"), collection: collection});
		};

		return {
			start: initialize
		};
		
	};


	return FlickrFeedModule;

});