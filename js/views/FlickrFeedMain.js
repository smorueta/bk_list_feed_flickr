//mainView.js
define(['backbone', 'js/models/flickrFeedModel', 'js/views/FlickrFeedItemListView', 'js/views/FlickrFeedItemDetailView'],function(Backbone, FlickrFeedModel, FlickrFeedItemListView, FlickrFeedItemDetailView){
	return Backbone.View.extend({

        initialize: function(options){
            _.bindAll(this, "renderItem");
            
            this.defineRegions();
            this.defineData(options);
            this.render();
        },

        defineRegions:function(){
            this.regions = {
                list: {
                    el: this.$('#list-items-container'),
                    view: FlickrFeedItemListView,
                    listenerEvent: {
                        name: 'clickOnItem',
                        action: this.onClickItemListener
                    }
                },
                detail: {
                    el: $('#detail-item'),
                    view: FlickrFeedItemDetailView,
                    listenerEvent: {
                        name: 'clickOnBackItem',
                        action: this.onClickItemListener
                    }
                }
            };
        },

        defineData:function (options) {
            this.collection = options.collection;
        },

        renderItem: function (currentRegion, data) {
            if(this.itemView){
                //remove currentView
                this.itemView.remove();

                //off events
               this.cleanEventsListeners(currentRegion);
            }

            this.itemView = new this.regions[currentRegion].view({data: data});
            this.regions[currentRegion].el.append(this.itemView.el);
            this.itemView.on(this.regions[currentRegion].listenerEvent.name, _.bind(this.regions[currentRegion].listenerEvent.action,this));
        },

        render: function (selectedModel) {
            if (!selectedModel){
                this.renderItem("list", this.collection);
            } else {
                this.renderItem("detail", selectedModel);
            }
        },

        onClickItemListener:function (model) {
            this.render(model || null);
        },

        cleanEventsListeners:function (currentRegion) {
            for (item in this.regions){
                if (currentRegion !== item) {
                    this.itemView.off(this.regions[item].listenerEvent.name, _.bind(this.regions[item].listenerEvent.action,this));   
                }
            }
        }
	});
});