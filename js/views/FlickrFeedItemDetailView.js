define(['backbone'],function(Backbone){
    return Backbone.View.extend({	

        template: 'js/views/templates/flickr-feed-item-detail-view.html',

        className: 'item-detail',

        events: {
        	'click [data-selector="back"]' : 'onClickBack'
        },

        initialize:function(options){
            this.model = options.data;

            (function(view){
                $.get( view.template, function (data) { 
                    template = _.template($(data).html(), {
                        escape: false
                    });
                    view.$el.html(template({item:view.model.toJSON()}));  
                }, 'html')
            })(this);

            this.animateView();
        },

        onClickBack:function(){
            this.trigger('clickOnBackItem');
        },

        animateView:function(){
            this.$el.animate({
                opacity: '1'
            }, 5000);
        }
    	
    });

});