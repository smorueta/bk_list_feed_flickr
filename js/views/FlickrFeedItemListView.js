define(['backbone', 'js/views/FlickrFeedItemView'],function(Backbone, FlickrFeedItemView){
	return Backbone.View.extend({	

        template: 'js/views/templates/flickr-feed-item-list-view.html',

        tagName: 'ul',

        className: 'list-feeds',


        initialize:function(options){
            this.collection = options.data;

            this.collection.each(function(model){
                this.renderItem.call(this, "list", model);
                this.defineListeners();
            }, this);   
			
        },

        renderItem: function(currentRegion, model){
            this.itemView = new FlickrFeedItemView({model: model});
            this.$el.append(this.itemView.el);
        }, 

        defineListeners:function(){
            this.itemView.on('clickOnItem', _.bind(this.onClickItemListener,this));
        },

        onClickItemListener:function(model){
            this.trigger('clickOnItem', model);
        }
		
	});

});