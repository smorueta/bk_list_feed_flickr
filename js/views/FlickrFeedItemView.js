define(['backbone'],function(Backbone){ 
	return Backbone.View.extend({	

        template: 'js/views/templates/flickr-feed-item-view.html',

        tagName: 'li',

        className: 'item-list',

        events: {
            'click [data-selector="see-detail"]': 'onClickItem',
            'mouseover [data-selector="see-detail"]': 'onMouseOverItem',
        	'mouseout [data-selector="see-detail"]': 'onMouseOutItem'
        },

        initialize:function(){
			(function(view){
				$.get( view.template, function (data) {				
                	template = _.template($(data).html());
                	view.$el.html(template({item:view.model.toJSON()}));  
            	}, 'html')
            })(this);

        }, 

        onClickItem:function(){
            this.$el.animate({
                left: '-96%'
            }, 500, _.bind(function(){
                this.trigger('clickOnItem', this.model)
            }, this));
        	
        },

        onMouseOverItem:function(){
            this.$('.container-arrow').addClass('block');
        },

        onMouseOutItem:function(){
            this.$('.container-arrow').removeClass('block');
        } 
		
	});

});