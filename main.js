require(['backbone', 'underscore', 'jquery', 'js/controllers/mainController'], function(Backbone, _, $, MainController){
    
    (function(){
		var mainController = new MainController();
    	mainController.start();
    })();
    
});