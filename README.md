# README #

### What is this repository for? ###

* This app is a basic backbone estructure, done to consume a public JSON API Feed from Flickr. The app shows a list of feeds and the details about every of them when the user click on them.

* Version 0.1

### How do I get set up? ###

The app needs a server node to be executed. It's included in the repository. To run it, open a terminal or windows console and located in the project directory write "node server.js". A instance of server node will be started on port 8000. Then, writing localhost:8000 in your browser, you will be able visit the website.
  
### Next planned upgrading ###

- Search items e.g. a free-text search box where text entered is matched against tags
- Infinite scrolling or "Load more" style pagination
- Google +1 button for results
- Build process for generating production-ready code
- Tests (unit tests, e2e tests, etc)